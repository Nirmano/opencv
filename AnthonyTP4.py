import cv2
import sys


print("Nom du script :", sys.argv[0])
print("Nombre d'argument :", len(sys.argv))
print("Les arguments sont :", str(sys.argv))

#cap = cv2.VideoCapture(0)

#print("Camera opened : " + str(cap.isOpened()))
src = sys.argv[1]
val = 3

while(True):
   # ret, frame = cap.read()
    frame = cv2.imread(src)

    
    cv2.imshow('Original',frame)

    frame = cv2.blur(frame,(val,val))
    cv2.imshow('Lissage Moyen',frame)
    
    frame = cv2.medianBlur(frame,val)
    cv2.imshow('Lissage Gaussien',frame)

    frame = cv2.GaussianBlur(frame,(val,val),val)
    cv2.imshow('Lissage Median',frame)
    
    key = cv2.waitKey(1)
    if key == 27:
        break
    elif key == 61:
        val +=2
    elif key == 45:
        if val-2 > 3:
            val -= 2


cv2.destroyAllWindows()
#cap.release()