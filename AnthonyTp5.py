import cv2
import sys
import numpy as np


print("Nom du script :", sys.argv[0])
print("Nombre d'argument :", len(sys.argv))
print("Les arguments sont :", str(sys.argv))

if len(sys.argv) > 1 :
    src = sys.argv[1]
else:
    src = cv2.VideoCapture(0)

mode = 1
lower = 0
upper = 180

while(True):
    if len(sys.argv) > 1 :
        frame = cv2.imread(src)
    else:
        ret, frame = src.read()

    cv2.imshow('Original',frame)
    hsv = cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)
    teinte,saturation,valeur = cv2.split(hsv)
    seg_hue = cv2.inRange(hsv,(lower,20,0),(upper,255,255))
    
    if mode == 1:
        #mode HSV
        cv2.imshow('HSV', hsv)
        cv2.imshow('Teinte',teinte)
        cv2.imshow('Saturation',saturation)
        cv2.imshow('valeur',valeur)
    else:
        #mode filtrée
        cv2.imshow('Filtre', seg_hue)
            


    key = cv2.waitKey(1)
    if key == 27:
        break
    elif key == 32:
        if mode == 1:
            mode = 2
            cv2.destroyWindow('HSV')
            cv2.destroyWindow('Teinte')
            cv2.destroyWindow('Saturation')
            cv2.destroyWindow('valeur')
        else:
            mode = 1
            cv2.destroyWindow('Filtre')
    elif key == 61:
        if upper < 180:
            upper += 1             
            print("Upper :", upper)      
    elif key == 45:
        if upper > 0:
            upper -= 1
            print("Upper :", upper)
    elif key == 48:
        if lower < 180:
            lower += 1
            print("Lower :", lower)
    elif key == 57:
        if lower > 0:
            lower -= 1
            print("Lower :", lower)


cv2.destroyAllWindows()