import cv2
import sys
import numpy as np


print("Nom du script :", sys.argv[0])
print("Nombre d'argument :", len(sys.argv))
print("Les arguments sont :", str(sys.argv))

if len(sys.argv) > 1 :
    src = sys.argv[1]
else:
    src = cv2.VideoCapture(0)

single = True

while(True):
    if len(sys.argv) > 1 :
        frame = cv2.imread(src)
    else:
        ret, frame = src.read()

    cv2.imshow('Original',frame)
    
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    sobelx64f = cv2.Sobel(gray, cv2.CV_64F, 1, 0)
    abs_sobelxf  = cv2.convertScaleAbs(sobelx64f)
    
    sobely64f = cv2.Sobel(gray, cv2.CV_64F, 0, 1)
    abs_sobelyf = cv2.convertScaleAbs(sobely64f)
   

    if len(sys.argv) > 1 :
        precedent = cv2.addWeighted(abs_sobelxf, 0.5, abs_sobelyf, 0.5, 0)
    else :
        now = cv2.addWeighted(abs_sobelxf, 0.5, abs_sobelyf, 0.5, 0)
        if single :
            precedent = now
            single = False

        precedent = cv2.addWeighted(precedent, 0.9, now, 0.1, 0)
   # sobel_8u = np.uint8(sobel_64f)


    cv2.imshow('Sobel de premier ordre en X', abs_sobelxf)
    cv2.imshow('Sobel de premier ordre en Y', abs_sobelyf)
    cv2.imshow('Sobel X + Y', precedent)
 


    key = cv2.waitKey(1)
    if key == 27:
        break
    
cv2.destroyAllWindows()